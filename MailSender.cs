﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace IEP_Auctions
{
    public class MailSender
    {   
        private static readonly string ePass = "iepprojekat18";
        private static readonly string eUser = "iepsm150336@gmail.com";
        
       public static void sendMail(string text, string subject, string recipient)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
            smtpClient.Port = 587;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(eUser, ePass);
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(eUser);
            mail.To.Add(new MailAddress(recipient));
            mail.Subject = subject;
            mail.Body = text;
            smtpClient.Send(mail);
        }

    }
}