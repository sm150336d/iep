﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using IEP_Auctions.Models;

namespace IEP_Auctions.Hubs
{
    public class BidHub : Hub
    {
        public static void InsertBid(string firstName, string lastName, int tokens, DateTime time, Guid auctionID)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<BidHub>();
            hubContext.Clients.All.AddBidToPage(firstName, lastName, tokens, time);
        }

        public static void ChangePrice(Guid auctionID, string email, float price, int tokens, string currency)
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<BidHub>();
            hubContext.Clients.All.UpdateBidPrice(auctionID, email, price, tokens, currency);
        }

    }
}