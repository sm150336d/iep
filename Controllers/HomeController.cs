﻿using IEP_Auctions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using PagedList;

namespace IEP_projekat.Controllers
{
    public class HomeController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public ActionResult Index(int? min, int? max, int? itemsPerPage, string status, string searchString, string currentFilter, int? page)
        {
            log.Info("Action Home/Index has been fired.");
            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                int items;
                //if applying search string durign paging, must restart from page 1
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;


                List<Auction> auctions = db.Auction.Include("Bid.User").Where(a => a.status.Trim().Equals("READY") == false).OrderByDescending(a => a.created).ToList();
                if (!String.IsNullOrEmpty(status) && !status.Equals("State"))
                {
                    auctions = auctions.Where(a => a.status.Trim().Equals(status) == true).ToList();
                }
                ViewBag.Status = status;

                if (!String.IsNullOrEmpty(searchString))
                {
                    string[] words = searchString.Split(' ');
                    foreach (string str in words) auctions = auctions.Where(a => a.name.ToLower().Contains(str.ToLower())).ToList();
                }

                if (min != null)
                {
                    auctions = auctions.Where(a => a.currentTokenPrice >= min).ToList();
                }
                ViewBag.Min = min;

                if (max != null)
                {
                    auctions = auctions.Where(a => a.currentTokenPrice <= max).ToList();
                }
                ViewBag.Max = max;

                if (itemsPerPage == null)
                {
                    items = db.AdminConfiguration.FirstOrDefault().itemsPerPage;
                }
                else items = (int)itemsPerPage;

                ViewBag.ItemsPerPage = items;


                int pageNumber = (page ?? 1);
                return View(auctions.ToPagedList(pageNumber, items));
            }
        }
    }
}