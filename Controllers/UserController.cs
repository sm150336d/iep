﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEP_Auctions.Models;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using PagedList;

namespace IEP_projekat.Controllers
{
    public class UserController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public ActionResult Register()
        {
            log.Info("Action User/Register has been fired.");
            if (Session["User"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Exclude = "tokens, verified, userType")] User user)
        {
            log.Info("Post action User/Register has been fired.");
            if (Session["User"] != null)
            {
                return RedirectToAction("Index", "Home");
            }

            bool status = false;
            string message = "";

            //validate model
            if (ModelState.IsValid)
            {
                //check if email already exists
                if (EmailExists(user.email))
                {
                    ModelState.AddModelError("EmailExists", "Email already exists");
                    return View(user);
                }

                //hash password
                user.password = Crypto.Hash(user.password);

                //set other parameters
                user.tokens = 0;
                user.verified = false;
                user.userType = "user";

                using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
                {
                    db.User.Add(user);
                    db.SaveChanges();
                }

                status = true;
                message = "Registration successful";
                
            }
            else
            {
                message = "Invalid request";
            }

            ViewBag.Message = message;
            ViewBag.Status = status;
            return View(user);
        }


        [HttpGet]
        public ActionResult Login()
        {
            log.Info("Action User/Login has been fired.");
            if (Session["User"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login login, string returnUrl="")
        {
            log.Info("Post action User/Login has been fired.");
            if (Session["User"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            string message = "";

            using(IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                var user = db.User.Where(u => u.email == login.email).FirstOrDefault();
                if (user == null)
                {
                    message = "Email does not exist";
                }
                else
                {
                    //user exists
                    if (string.Compare(Crypto.Hash(login.password), user.password) == 0)
                    {
                        Session["UserID"] = user.userID;
                        Session["FirstName"] = user.firstName;
                        Session["User"] = user;
                        return RedirectToAction("Index", "Home");

                    }
                    else
                    {
                        message = "Invalid password";
                    }
                }
            }

            ViewBag.Message = message;
            return View();
        }


        public ActionResult Logout()
        {
            log.Info("Action User/Logout has been fired.");
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        [NonAction]
        public bool EmailExists(string email)
        {
            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                var exists = db.User.Where(u => u.email == email).FirstOrDefault();
                return exists != null;
            }
        }

        [HttpGet]
        public ActionResult ProfilePage()
        {
            log.Info("Action User/ProfilePage has been fired.");
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            User user = (User)Session["User"];
            ViewBag.UserType = user.userType.Trim();

            UserView uv = new UserView((Guid)Session["userID"]);
            return View(uv);
        }

        [HttpGet]
        public ActionResult EditUser()
        {
            log.Info("Action User/EditUser has been fired.");
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            UserEdit ue = new UserEdit((Guid)Session["userID"]);
            return View(ue);
        }


        [HttpPost]
        public ActionResult EditUser(UserEdit ue)
        {
            log.Info("Post action User/EditUser has been fired.");
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            User user = IEP_Auctions.Models.User.GetUserByID((Guid)Session["userID"]);

            bool status = false;
            string message = "";

            if (ModelState.IsValid)
            {
                var oldPassword = Crypto.Hash(ue.oldPassword);

                if (string.Compare(user.password, oldPassword) != 0)
                {
                    message = "Incorrect password";
                    status = false;
                }
                else
                {
                    using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
                    {
                        if (ue.email != user.email)
                        {
                            if (EmailExists(ue.email))
                            {
                                ModelState.AddModelError("EmailExists", "Email already exists");
                                return View(ue);
                            }
                            user.email = ue.email;
                        }

                        if (ue.newPassword != null)
                        {
                            ue.newPassword = Crypto.Hash(ue.newPassword);
                            user.password = ue.newPassword;
                        }

                        user.firstName = ue.firstName;
                        user.lastName = ue.lastName;

                        Session["User"] = user;
                        Session["FirstName"] = user.firstName;

                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    status = true;
                    message = "Edit successful";
                 }

            }
            else
            {
                message = "Invalid request";
            }

            ViewBag.Message = message;
            ViewBag.Status = status;
            return View(ue);
        }


        [HttpGet]
        public ActionResult EditConfiguration()
        {
            log.Info("Action User/EditConfiguration has been fired.");
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            User user = (User)Session["User"];
            if (!user.userType.Trim().Equals("admin")) return RedirectToAction("Index", "Home");


            AdminConfiguration ac;
            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                ac = db.AdminConfiguration.FirstOrDefault();
            }

            AdminConfigurationEdit ace = new AdminConfigurationEdit(ac);
            return View(ace);
        }

        [HttpPost]
        public ActionResult EditConfiguration(AdminConfigurationEdit ace)
        {
            log.Info("Post action User/EditConfiguration has been fired.");
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "User");
            }

            User user = (User)Session["User"];

            if (!user.userType.Trim().Equals("admin")) return RedirectToAction("Login", "User");

            bool status = false;
            string message = "";

            //validate model
            if (ModelState.IsValid)
            {
                var password = Crypto.Hash(ace.password);

                if (string.Compare(user.password, password) != 0)
                {
                    message = "Incorrect password";
                    status = false;
                }
                else if (ace.silverPack >= ace.goldPack || ace.goldPack >= ace.platinumPack)
                {
                    message = "Invalid token pack values. The order must be Silver < Gold < Platinum";
                    status = false;
                }
                else
                {
                    using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
                    {
                        AdminConfiguration ac = db.AdminConfiguration.FirstOrDefault();

                        ac.auctionDuration = ace.auctionDuration;
                        ac.currency = ace.currency;
                        ac.tokenValue = ace.tokenValue;
                        ac.itemsPerPage = ace.itemsPerPage;
                        ac.silverPack = ace.silverPack;
                        ac.goldPack = ace.goldPack;
                        ac.platinumPack = ace.platinumPack;

                        db.Entry(ac).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    status = true;
                    message = "Edit successful";
                }

            }
            else
            {
                message = "Invalid request";
            }

            ViewBag.Message = message;
            ViewBag.Status = status;
            return View(ace);
        }


        public ActionResult WonAuctions(int? min, int? max, int? itemsPerPage, string searchString, string currentFilter, int? page)
        {
            log.Info("Action User/WonAuctions has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");

            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                int items;
                //if applying search string durign paging, must restart from page 1
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;


                Guid userID = (Guid)Session["UserID"];
                User user = (User)Session["User"];
                ViewBag.Email = user.email;

                List<Auction> auctions = IEP_Auctions.Models.User.GetWonAuctions(userID); 

                if (!String.IsNullOrEmpty(searchString))
                {
                    string[] words = searchString.Split(' ');
                    foreach (string str in words) auctions = auctions.Where(a => a.name.ToLower().Contains(str.ToLower())).ToList();
                }

                if (min != null)
                {
                    auctions = auctions.Where(a => a.currentTokenPrice >= min).ToList();
                }
                ViewBag.Min = min;

                if (max != null)
                {
                    auctions = auctions.Where(a => a.currentTokenPrice <= max).ToList();
                }
                ViewBag.Max = max;

                if (itemsPerPage == null)
                {
                    items = db.AdminConfiguration.FirstOrDefault().itemsPerPage;
                }
                else items = (int)itemsPerPage;

                ViewBag.ItemsPerPage = items;


                int pageNumber = (page ?? 1);
                return View(auctions.ToPagedList(pageNumber, items));
            }
        }


        [HttpGet]
        public ActionResult MyAuctions(int? min, int? max, int? itemsPerPage, string searchString, string currentFilter, int? page)
        {
            log.Info("Action User/MyAuctions has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");

            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                int items;

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;


                Guid userID = (Guid)Session["UserID"];

                List<Auction> auctions = db.Auction.Include("Bid.User").Where(a => a.userID == userID).ToList();

                if (!String.IsNullOrEmpty(searchString))
                {
                    string[] words = searchString.Split(' ');
                    foreach (string str in words) auctions = auctions.Where(a => a.name.ToLower().Contains(str.ToLower())).ToList();
                }

                if (min != null)
                {
                    auctions = auctions.Where(a => a.currentTokenPrice >= min).ToList();
                }
                ViewBag.Min = min;

                if (max != null)
                {
                    auctions = auctions.Where(a => a.currentTokenPrice <= max).ToList();
                }
                ViewBag.Max = max;

                if (itemsPerPage == null)
                {
                    items = db.AdminConfiguration.FirstOrDefault().itemsPerPage;
                }
                else items = (int)itemsPerPage;

                ViewBag.ItemsPerPage = items;


                int pageNumber = (page ?? 1);
                return View(auctions.ToPagedList(pageNumber, items));
            }
        }


    }

}