﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Reflection;
using System.Net;
using IEP_Auctions.Hubs;
using IEP_Auctions.Models;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading;
using Microsoft.AspNet.Identity;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;
using System.Text.RegularExpressions;

namespace IEP_projekat.Controllers
{
    public class AuctionController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Database db = new Database();

        [HttpGet]
        //[Authorize]
        public ActionResult CreateAuction()
        {
            log.Info("Action Auction/CreateAuction has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");
            AdminConfiguration ac = db.AdminConfiguration.FirstOrDefault();
            ViewBag.Currency = ac.currency;
            return View();
        }

        [HttpPost]
        //[Authorize]
        public ActionResult CreateAuction(AuctionMetadata am)
        {
            log.Info("Post action Auction/CreateAuction has been fired.");
            string message = "";
            bool status = false;

            if (Session["User"] == null) return RedirectToAction("Login", "User");

            //validate model
            if (am.imageFile != null && ModelState.IsValid)
            { 
                var config = db.AdminConfiguration.FirstOrDefault();

                byte[] image = new byte[am.imageFile.ContentLength];
                am.imageFile.InputStream.Read(image, 0, image.Length);

                Auction auction = new Auction();
                User user = (User)Session["User"];
                auction.userID = user.userID;
                auction.created = DateTime.Now;
                auction.status = "READY";
                if (am.duration == null) auction.duration = config.auctionDuration;
                else auction.duration = (long)am.duration;
                auction.name = am.name;
                auction.startingPrice = (float)am.startingPrice;
                auction.currentPrice = auction.startingPrice;
                auction.currentTokenPrice = (int)Math.Ceiling(am.startingPrice / config.tokenValue);
                auction.currency = config.currency;
                auction.tokenValue = config.tokenValue;
                auction.image = image;

                using (Database db = new Database())
                {
                    db.Auction.Add(auction);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        foreach (var entityValidationErrors in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in entityValidationErrors.ValidationErrors)
                            {
                                Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                            }
                        }
                    }
                }
                status = true;
                message = "Creating auction successful";

            }
            else
            {
                message = "Invalid request";
            }

            ViewBag.Message = message;
            ViewBag.Status = status;
            return View(am);
        }

        [HttpGet]
        public ActionResult BidAuction(Guid? auctionID) 
        {
            log.Info("Action Auction/BidAuction has been fired.");
            if (auctionID == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Session["User"] != null)
            {
                User user = (User)Session["User"];
                if (user.userType.Trim().Equals("user")) ViewBag.LoggedIn = 1;
                else ViewBag.LoggedIn = 2;
            }
            else ViewBag.LoggedIn = 0;

            if (TempData["PDF"] != null) ViewBag.PDF = true;

            AuctionView av = new AuctionView((Guid)auctionID);
            return View(av);
        }

        [HttpPost]
        public ActionResult BidAuction(int? amount, Guid? auctionID)
        {
            log.Info("Post action Auction/BidAuction has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");

            if (auctionID == null || amount == null)
            {
                return RedirectToAction("Index", "Home");
            }

            string message = "";
            bool success = false;

            SqlConnection MyConnection = new SqlConnection("Server=tcp:serversm150336.database.windows.net,1433;Initial Catalog=IEPsm150336db;Persist Security Info=False;User ID=sm150336;Password=B5b614D7376b;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

            SqlDataAdapter MyDataAdapter = new SqlDataAdapter("spCreateBid", MyConnection);

            MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@UserID", SqlDbType.UniqueIdentifier));
            MyDataAdapter.SelectCommand.Parameters["@UserID"].Value = (Guid)Session["UserID"];

            MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@AuctionID", SqlDbType.UniqueIdentifier));
            MyDataAdapter.SelectCommand.Parameters["@AuctionID"].Value = (Guid) auctionID;

            MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@Tokens", SqlDbType.Int));
            MyDataAdapter.SelectCommand.Parameters["@Tokens"].Value = (int) amount;

            MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@StatusCode", SqlDbType.Int));
            MyDataAdapter.SelectCommand.Parameters["@StatusCode"].Direction = ParameterDirection.Output;

            int status = Bid.ExecuteInsertBid(MyDataAdapter);
            MyConnection.Close();

            if (status == 0) Bid.AlertBidInsert((Guid)auctionID, (Guid)Session["UserID"], (int)amount);

            switch (status)
            {
                case 0:
                    message = "Successfull bidding!";
                    success = true;
                    break;
                case 1:
                    message = "You do not have enough tokens!";
                    break;
                case 2:
                    message = "Higher bid exists!";
                    break;
                case 3:
                    message = "The auction is completed!";
                    break;
            }
            
            ViewBag.Message = message;
            ViewBag.Success = success;
            AuctionView av = new AuctionView((Guid)auctionID);
            return View(av);
        }

        //[AjaxOnly]
        public void CompleteAuction(Guid auctionID)
        {
            if (Request.IsAjaxRequest())
            {
                Auction.CompleteAuction(auctionID);
            }
        }

        public ActionResult StartAuction(Guid? auctionID)
        {
            log.Info("Action Auction/StartAuction has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");
            User user = (User)Session["User"];
            if (!user.userType.Trim().Equals("admin")) return RedirectToAction("Login", "User");
            if (auctionID == null) return RedirectToAction("Index", "Home");

            int status = Auction.StartAuction((Guid)auctionID);
            string adminMessage = "";
            bool adminStatus = false;

            switch (status)
            {
                case 0:
                    adminMessage = "Auction has been started successfully!";
                    adminStatus = true;
                    break;
                case 1:
                    adminMessage = "Auction could not be started! It has either already started or is completed.";
                    break;
            }

            ViewBag.AdminMessage = adminMessage;
            ViewBag.AdminStatus = adminStatus;

            return RedirectToAction("BidAuction", auctionID);

        }

        public ActionResult DeleteAuction(Guid? auctionID)
        {
            log.Info("Action Auction/DeleteAuction has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");
            User user = (User)Session["User"];
            if (!user.userType.Trim().Equals("admin")) return RedirectToAction("Login", "User");
            if (auctionID == null) return RedirectToAction("Index", "Home");

            int status = Auction.DeleteAuction((Guid)auctionID);
            string adminMessage = "";
            bool adminStatus = false;

            switch (status)
            {
                case 0:
                    adminMessage = "Auction has been deleted successfully!";
                    adminStatus = true;
                    break;
                case 1:
                    adminMessage = "Auction could not be deleted! It has either already started or is completed.";
                    break;
            }

            ViewBag.AdminMessage = adminMessage;
            ViewBag.AdminStatus = adminStatus;

            return View();
        }

        [HttpGet]
        public ActionResult ReviewAuctions()
        {
            log.Info("Action Auction/ReviewAuctions has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");
            User user = (User)Session["User"];
            if (!user.userType.Trim().Equals("admin")) return RedirectToAction("Login", "User");

            using (Database db = new Database())
            {
                List<Auction> auctions = db.Auction.Where(a => a.status.Trim().Equals("READY") == true).ToList();
                return View(auctions);
            }

        }

        [HttpGet]
        public ActionResult SingleAuctionReview(Guid? auctionID)
        {
            log.Info("Action Auction/SingleAuctionReview has been fired.");
            if (auctionID == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Session["User"] == null) return RedirectToAction("Login", "User");
            else 
            {
                User user = (User)Session["User"];
                if (!user.userType.Trim().Equals("admin")) return RedirectToAction("Index", "Home");
            }

            AuctionView av = new AuctionView((Guid)auctionID);
            return View(av);
        }

        [NonAction]
        public string RenderViewAsString(string viewName, object model)
        {
            // create a string writer to receive the HTML code
            StringWriter stringWriter = new StringWriter();

            // get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext,
                      viewName, null);
            // create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                new ViewDataDictionary(model),
                new TempDataDictionary(),
                stringWriter
            );

            // render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            // return the HTML code
            return stringWriter.ToString();
        }

        [HttpPost]
        public ActionResult PrintPDF(Guid? auctionID)
        {
            if (auctionID == null) return RedirectToAction("Index", "Home");

            AuctionView av = new AuctionView((Guid)auctionID);

            /*string path = "";

            var t = new Thread((ThreadStart)(() => {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.RootFolder = System.Environment.SpecialFolder.MyComputer;
                fbd.ShowNewFolderButton = true;
                if (fbd.ShowDialog() == DialogResult.Cancel) path = null;
                else path = fbd.SelectedPath;
            }));

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();*/



            // get the HTML code of this view
            string html = RenderViewAsString("BidAuction", av);
            html = Regex.Replace(html, @"<body>(.|\n)*<h2>", "<body>\n<div class='container body - content'>\n<h2>");
            html = Regex.Replace(html, @"pocetak(.|\n)*kraj", "");
            html = Regex.Replace(html, @"izbaci(.|\n)*ovo", "");
            html = Regex.Replace(html, @"izbrisi1(.|\n)*izbrisi2", "");
            html = Regex.Replace(html, @"izbrisi3(.|\n)*izbrisi4", "");

            // the base URL to resolve relative images and css
            //String thisPageUrl = this.ControllerContext.HttpContext.Request.Url.AbsoluteUri;
            //String baseUrl = thisPageUrl.Substring(0, thisPageUrl.Length - "Auction/PrintPDF".Length);


            Byte[] bytes;

            using (var ms = new MemoryStream())
            {

                using (var doc = new Document())
                {

                    using (var writer = PdfWriter.GetInstance(doc, ms))
                    {

                        doc.Open();
                        writer.CloseStream = false;


                        using (var srHtml = new StringReader(html))
                        {
                            iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);
                        }

                        byte[] imageBytes = av.auction.image;
                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageBytes);
                        image.ScaleToFit(300f, 300f);
                        doc.Add(image);

                        doc.Close();
                    }
                }


                bytes = ms.ToArray();
            }

            var fileName = "Auction_" + auctionID.ToString() + ".pdf";
            var path = Path.GetTempPath();

            //var path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); //radi u lokalu

            var testFile = Path.Combine(path, fileName);
            //System.IO.File.WriteAllBytes(testFile, bytes);


            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();



            TempData["PDF"] = true;
            return RedirectToAction("BidAuction", new { auctionID = auctionID });
        }



    }

}