﻿using IEP_Auctions.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEP_Auctions.Controllers
{
    public class TokenOrderController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [HttpGet]
        public ActionResult OrderTokens()
        {
            log.Info("Action TokenOrder/OrderTokens has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");

            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                AdminConfiguration ac = db.AdminConfiguration.FirstOrDefault();

                ViewBag.Silver = ac.silverPack;
                ViewBag.Gold = ac.goldPack;
                ViewBag.Platinum = ac.platinumPack;

                ViewBag.SilverPrice = ac.silverPack * ac.tokenValue;
                ViewBag.GoldPrice = ac.goldPack * ac.tokenValue;
                ViewBag.PlatinumPrice = ac.platinumPack * ac.tokenValue;

                ViewBag.Currency = ac.currency;

            }

            return View();
        }


        [HttpPost]
        public ActionResult OrderTokens(string pack)
        {
            log.Info("Post action TokenOrder/OrderTokens has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");

            string message = "";
            bool status = false;

            if (pack == null)
            {
                message = "No token pack selected";
            }
            else
            {
                Guid userID = (Guid)Session["UserID"];
                Guid orderID = IEP_Auctions.Models.TokenOrder.createTokenOrder(userID, pack);

                return Redirect("http://stage.centili.com/payment/widget?apikey=e0b8e3565eaceec89e9077f490848a23&country=rs&reference=" + orderID);
            }

            ViewBag.Message = message;
            ViewBag.Status = status;

            return View();
        }


        public ActionResult SuccessfulPayment(Guid? reference)
        {
            log.Info("Action TokenOrder/SuccessfulPayment has been fired.");
            IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database();

            using (var trans = db.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    TokenOrder order = db.TokenOrder.Find(reference);

                    if (order != null && order.state.Trim().Equals("SUBMITTED"))
                    {
                        User user = db.User.Find(order.userID);

                        order.state = "COMPLETED";
                        user.tokens += order.tokens;
                        db.Entry(user).State = EntityState.Modified;
                        db.Entry(order).State = EntityState.Modified;
                        db.SaveChanges();
                        trans.Commit();

                        string mailBody = $"Dear {user.firstName}, your purchase of {order.tokens} tokens succeeded. Thank you for using our services.";
                        MailSender.sendMail(mailBody, "Token Purchase", user.email);

                    }
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    log.Error("Transaction failed");
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult FailedPayment(Guid? reference)
        {
            log.Info("Action TokenOrder/FailedPayment has been fired.");

            if (reference == null) return RedirectToAction("Index", "Home");

            IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database();
            TokenOrder order = db.TokenOrder.Find(reference);
            if (order != null && order.state.Trim().Equals("SUBMITTED"))
            {
                order.state = "CANCELED";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                User user = db.User.Find(order.userID);
                string mailBody = $"Dear {user.firstName}, your purchase of {order.tokens} tokens failed. Please try again";
                MailSender.sendMail(mailBody, "Token Purchase", user.email);
            }

            return RedirectToAction("Index", "Home");
        }


        public ActionResult TokenOrders(int? itemsPerPage, string searchString, string currentFilter, int? page)
        {
            log.Info("Action TokenOrder/TokenOrders has been fired.");
            if (Session["User"] == null) return RedirectToAction("Login", "User");

            using (IEP_Auctions.Models.Database db = new IEP_Auctions.Models.Database())
            {
                int items;
                if (searchString != null && !searchString.Equals("State"))
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;


                Guid userID = (Guid)Session["UserID"];
                User user = (User)Session["User"];
                ViewBag.Email = user.email;

                List<TokenOrder> tokenOrders = IEP_Auctions.Models.TokenOrder.getTokenOrders(userID);

                if (!String.IsNullOrEmpty(searchString))
                {
                    tokenOrders = tokenOrders.Where(a => a.state.Trim().Equals(searchString)).ToList();
                }

                if (itemsPerPage == null)
                {
                    items = db.AdminConfiguration.FirstOrDefault().itemsPerPage;
                }
                else items = (int)itemsPerPage;

                ViewBag.ItemsPerPage = items;


                int pageNumber = (page ?? 1);
                return View(tokenOrders.ToPagedList(pageNumber, items));
            }
        }

    }
}