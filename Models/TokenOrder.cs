namespace IEP_Auctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TokenOrder")]
    public partial class TokenOrder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid orderID { get; set; }

        public Guid userID { get; set; }

        public int tokens { get; set; }

        public float price { get; set; }

        [Required]
        [StringLength(10)]
        public string state { get; set; }

        [Required]
        [StringLength(50)]
        public string currency { get; set; }

        public virtual User User { get; set; }
    }
}
