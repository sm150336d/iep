namespace IEP_Auctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bid")]
    public partial class Bid
    {
        public Guid bidID { get; set; }

        public Guid userID { get; set; }

        public Guid auctionID { get; set; }

        public int tokens { get; set; }

        public DateTime? time { get; set; }

        public bool? winner { get; set; }

        public virtual Auction Auction { get; set; }

        public virtual User User { get; set; }
    }
}
