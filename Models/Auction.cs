namespace IEP_Auctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Auction")]
    public partial class Auction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Auction()
        {
            Bid = new HashSet<Bid>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid auctionID { get; set; }

        public Guid userID { get; set; }

        [Required]
        public string name { get; set; }

        [Required]
        public byte[] image { get; set; }

        public long duration { get; set; }

        public float startingPrice { get; set; }

        public float? currentPrice { get; set; }

        [StringLength(50)]
        public string currency { get; set; }

        public DateTime? created { get; set; }

        public DateTime? opened { get; set; }

        public DateTime? closed { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        public float? tokenValue { get; set; }

        public int? currentTokenPrice { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bid> Bid { get; set; }
    }
}
