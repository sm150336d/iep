﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace IEP_Auctions.Models
{
    public partial class TokenOrder
    {
        public static List<TokenOrder> getTokenOrders(Guid userID)
        {
            using (Database db = new Models.Database())
            {
                return db.TokenOrder.Where(to => to.userID == userID).ToList();
            }
        }

        public static Guid createTokenOrder(Guid userID, string pack)
        {
            using (Database db = new Models.Database())
            {
                TokenOrder order = new TokenOrder();
                AdminConfiguration ac = db.AdminConfiguration.FirstOrDefault();

                switch (pack)
                {
                    case "silver":
                        order.tokens = ac.silverPack;
                        order.price = ac.silverPack * ac.tokenValue;
                        order.currency = ac.currency;
                        break;
                    case "gold":
                        order.tokens = ac.goldPack;
                        order.price = ac.goldPack * ac.tokenValue;
                        order.currency = ac.currency;
                        break;
                    case "platinum":
                        order.tokens = ac.platinumPack;
                        order.price = ac.platinumPack * ac.tokenValue;
                        order.currency = ac.currency;
                        break;
                }
                order.state = "SUBMITTED";
                order.userID = userID;

                db.TokenOrder.Add(order);
                db.SaveChanges();

                return order.orderID;
            }
        }

    }
}