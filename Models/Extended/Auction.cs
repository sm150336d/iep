﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
using System.Data.Entity;
using Microsoft.AspNet.SignalR;
using IEP_Auctions.Hubs;

namespace IEP_Auctions.Models
{

    public partial class Auction
    {
        public static Auction GetAuction(Guid auctionID)
        {
            using (Database db = new Models.Database())
            {
                Auction auction = db.Auction.Where(a => a.auctionID == auctionID).FirstOrDefault();
                return auction;
            }
        }

        public static void CompleteAuction(Guid auctionID)
        {
            using (Database db = new Database())
            {
                Auction auction = db.Auction.Where(a => a.auctionID == auctionID).FirstOrDefault();
                if (!auction.status.Equals("COMPLETED"))
                {
                    auction.status = "COMPLETED";
                    auction.closed = DateTime.Now;

                    Bid bid = db.Bid.Where(b => b.auctionID == auctionID).OrderByDescending(b => b.tokens).FirstOrDefault();
                    if (bid != null)
                    {
                        bid.winner = true;
                        db.Entry(bid).State = EntityState.Modified;
                    }
                    db.Entry(auction).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public static int StartAuction(Guid auctionID)
        {
            using (Database db = new Database())
            {
                Auction auction = db.Auction.Where(a => a.auctionID == auctionID).FirstOrDefault();
                if (auction.status.Equals("READY"))
                {
                    auction.status = "OPENED";
                    auction.opened = DateTime.Now;

                    db.Entry(auction).State = EntityState.Modified;
                    db.SaveChanges();

                    return 0;
                }
                else return 1;
            }
        }

        public static int DeleteAuction(Guid auctionID)
        {
            using (Database db = new Database())
            {
                Auction auction = db.Auction.Where(a => a.auctionID == auctionID).FirstOrDefault();
                if (auction.status.Equals("READY"))
                {

                    db.Auction.Remove(auction);
                    db.SaveChanges();

                    return 0;
                }
                else return 1;
            }
        }
    }

    public class AuctionMetadata
    {
        [Display(Name = "Auction name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in auction name")]
        public string name { get; set; }

        [Display(Name = "Image")]
        [Required(ErrorMessage = "Insert image")]
        public HttpPostedFileBase imageFile { get; set; }

        [Display(Name = "Duration")]
        [DataType(DataType.Duration)]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public long? duration { get; set; }

        [Display(Name = "Starting price")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in starting price")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public float startingPrice { get; set; }

    }


    public class AuctionView
    {
        public Auction auction { get; set; }
        public List<Bid> bids { get; set; }
        public User user { get; set; }

        public AuctionView(Guid auctionID)
        {
            auction = Auction.GetAuction(auctionID);
            bids = Bid.GetBids(auctionID);
            user = User.GetUser(auctionID);
        }
    }

}