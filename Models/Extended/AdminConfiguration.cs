﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IEP_Auctions.Models
{
	public class AdminConfigurationEdit
    {
        [Display(Name = "Silver pack")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in silver pack tokens")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public int silverPack { get; set; }

        [Display(Name = "Gold pack")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in gold pack tokens")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public int goldPack { get; set; }

        [Display(Name = "Platinum pack")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in platinum pack tokens")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public int platinumPack { get; set; }

        [Display(Name = "Items per page")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in items per page")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public int itemsPerPage { get; set; }

        [Display(Name = "Default auction duration")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in auction duration")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public long auctionDuration { get; set; }

		[Display(Name = "Currency")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in currency")]
        public string currency { get; set; }

        [Display(Name = "Token value")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in token value")]
        [RegularExpression(@"^[0-9]*[1-9][0-9]*$", ErrorMessage = "Must be a positive number")]
        public float tokenValue { get; set; }

        [Display(Name = "Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in password")]
        [DataType(DataType.Password)]
        [MinLength(7, ErrorMessage = "7 characters minimum")]
        public string password { get; set; }

        public AdminConfigurationEdit(AdminConfiguration ac)
        {
            silverPack = ac.silverPack;
            goldPack = ac.goldPack;
            platinumPack = ac.platinumPack;
            auctionDuration = ac.auctionDuration;
            itemsPerPage = ac.itemsPerPage;
            tokenValue = ac.tokenValue;
            currency = ac.currency;
        }

        public AdminConfigurationEdit()
        {

        }
    }
}