﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace IEP_Auctions.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        public static User GetUser(Guid auctionID)
        {
            using (Database db = new Models.Database())
            {
                Guid userID = db.Auction.Where(a => a.auctionID == auctionID).FirstOrDefault().userID;
                return db.User.Where(u => u.userID == userID).FirstOrDefault();
            }
        }

        public static User GetUserByID(Guid userID)
        {
            using (Database db = new Models.Database())
            {
                return db.User.Where(u => u.userID == userID).FirstOrDefault();
            }
        }

        public static List<Auction> GetWonAuctions(Guid userID)
        {
            using (Database db = new Models.Database())
            {
                bool temp = true;
                List<Auction> auctions = db.Bid.Include(b => b.Auction).Where(b => b.userID == userID && b.winner == temp).OrderByDescending(b => b.time).Select(b => b.Auction).ToList();
                return auctions;
            }
        }

        public static bool investTokens(int amount, Guid userID)
        {
            using (Database db = new Models.Database())
            {
                User user = db.User.Where(u => u.userID == userID).FirstOrDefault();
                int tokens = (int)user.tokens;
                if (tokens < amount) return false;
                user.tokens -= amount;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
        }

    }

    public class UserMetadata
    {
        [Display(Name = "First name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in first name")]
        public string firstName { get; set; }


        [Display(Name = "Last name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in last name")]
        public string lastName { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in email")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name = "Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in password")]
        [DataType(DataType.Password)]
        [MinLength(7, ErrorMessage = "7 characters minimum")]
        public string password { get; set; }
    }

    public class UserEdit
    {
        [Display(Name = "First name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in first name")]
        public string firstName { get; set; }

        [Display(Name = "Last name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in last name")]
        public string lastName { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in email")]
        public string email { get; set; }

        [Display(Name = "New password")]
        [DataType(DataType.Password)]
        [MinLength(7, ErrorMessage = "7 characters minimum")]
        public string newPassword { get; set; }

        [Display(Name = "Old password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fill in password")]
        [DataType(DataType.Password)]
        [MinLength(7, ErrorMessage = "7 characters minimum")]
        public string oldPassword { get; set; }

        public UserEdit(Guid userID)
        {
            User user = User.GetUserByID(userID);

            firstName = user.firstName;
            lastName = user.lastName;
            email = user.email;
        }

        public UserEdit()
        {

        }
    }

    public class UserView
    {
        [Display(Name = "First name")]
        public string firstName { get; set; }

        [Display(Name = "Last name")]
        public string lastName { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Tokens")]
        public int? tokens { get; set; }

        [Display(Name = "Type of user")]
        public string userType { get; set; }

        public UserView(Guid userID)
        {
            User user = User.GetUserByID(userID);

            firstName = user.firstName;
            lastName = user.lastName;
            tokens = user.tokens;
            userType = user.userType;
            email = user.email;
        }

    }
}