﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using IEP_Auctions.Hubs;
using Microsoft.AspNet.SignalR;
using System.Runtime.CompilerServices;
using System.Data;
using System.Data.SqlClient;

namespace IEP_Auctions.Models
{
    public partial class Bid
    {

        public static List<Bid> GetBids(Guid auctionID)
        {
            using (Database db = new Models.Database())
            {
                List<Bid> bids = db.Bid.Include(b => b.User).Where(b => b.auctionID == auctionID).OrderByDescending(b => b.tokens).ToList();
                return bids;
            }
        }

        public static bool AlertBidInsert(Guid auctionID, Guid userID, int amount)
        {
            using (Database db = new Database())
            {
                Bid bid = db.Bid.Include(b => b.Auction).Where(b => b.auctionID == auctionID && b.userID == userID && b.tokens == amount).FirstOrDefault();
                User user = db.User.Where(u => u.userID == userID).FirstOrDefault();
                Auction auction = db.Auction.Where(a => a.auctionID == auctionID).FirstOrDefault();
                float price = amount * (float)bid.Auction.tokenValue;

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<BidHub>();
                hubContext.Clients.All.AddBidToPage(user.firstName, user.lastName, amount, bid.time);
                hubContext.Clients.All.UpdateBidPrice(auctionID, user.email, price, amount, auction.currency);
            }

            return true;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static int ExecuteInsertBid(SqlDataAdapter MyDataAdapter)
        {
            DataSet DS = new DataSet();
            MyDataAdapter.Fill(DS, "spCreateBid");

            int status = (int)MyDataAdapter.SelectCommand.Parameters["@StatusCode"].Value;

            MyDataAdapter.Dispose();
            return status;
        }
    }
}