namespace IEP_Auctions.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AdminConfiguration")]
    public partial class AdminConfiguration
    {
        [Key]
        public int configurationID { get; set; }

        public int silverPack { get; set; }

        public int goldPack { get; set; }

        public int platinumPack { get; set; }

        public int itemsPerPage { get; set; }

        public long auctionDuration { get; set; }

        [Required]
        [StringLength(50)]
        public string currency { get; set; }

        public float tokenValue { get; set; }
    }
}
