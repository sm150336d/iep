namespace IEP_Auctions.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Database : DbContext
    {
        public Database()
            : base("name=Database")
        {
        }

        public virtual DbSet<AdminConfiguration> AdminConfiguration { get; set; }
        public virtual DbSet<Auction> Auction { get; set; }
        public virtual DbSet<Bid> Bid { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TokenOrder> TokenOrder { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Auction>()
                .HasMany(e => e.Bid)
                .WithRequired(e => e.Auction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TokenOrder>()
                .Property(e => e.state)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.userType)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .HasMany(e => e.Auction)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Bid)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TokenOrder)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
